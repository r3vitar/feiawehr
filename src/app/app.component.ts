import { registerLocaleData } from '@angular/common';
import { Component, OnDestroy, OnInit } from '@angular/core';
import localeDe from '@angular/common/locales/de';
import { Observable, Subscription } from 'rxjs';
import { DataService } from './data.service';
import { districts, DistrictType, getDistrictName, ListType, Nav } from './stuff';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnDestroy, OnInit {
  title = 'ff';

  set selectedBezirk(selectedBezirk: DistrictType | undefined) {
    this.data.setBezirk(selectedBezirk);
    this.doStuff()
  }

  get selectedBezirk() {
    return this.data.bz;
  }

  subs: Subscription[] = [];

  bezirke = [
    {
      id: undefined,
      text: "Keine Angabe"
    },
    ...districts.map((district => {
      return {
        id: district,
        text: getDistrictName(district as DistrictType)
      }
    }))
  ]

  nav = Nav;

  laufend?: Observable<number | undefined>;
  last6?: Observable<number | undefined>;
  today?: Observable<number | undefined>;
  last2?: Observable<number | undefined>;

  constructor(private data: DataService) {}
  ngOnDestroy(): void {
    this.subs.forEach(sub => sub.unsubscribe())
  }
  ngOnInit(): void {
    registerLocaleData(localeDe);

    this.doStuff()
  }

  doStuff() {
    this.subs.forEach(sub => sub.unsubscribe())
    this.subs = [];
    this.laufend = this.data.getCount(ListType.CURRENT);
    this.subs.push(this.laufend.subscribe())
  }
}
