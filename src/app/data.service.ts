import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Details, DistrictType, ListType, GET_BZ, Overview, URL_FOR_DETAILS, URL_FOR_LIST, EinsatzArrayItem } from './stuff';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  headers = {
    "X-Host-Override": "cf-intranet.ooelfv.at",
    "Accept": "*/*",
    "X-Referer-Override": "http://ooe.martinhochreiter.at/",
    "Accept-Language": "en-US,en;q=0.9,de-DE;q=0.8,de;q=0.7",
    "Access-Control-Allow-Headers": "*",
    "access-control-allow-origin": "*",
    "access-control-max-age": "3628800",
    "access-control-allow-methods": "GET",


  }

  bezirkChangeHandler: Array<{(): any}> = [];

  private _bz: DistrictType | undefined;

  get bz(): DistrictType | undefined {
    return this._bz
  }

  set bz(bz: DistrictType | undefined) {
    localStorage.setItem("bz", JSON.stringify({bz}));

    this._bz = bz;

    this.bezirkChangeHandler.forEach(bzh => bzh());
  }

  precount = 13 + 1;
  postcount = this.precount + 2;

  constructor(private http: HttpClient) {
    const bz = localStorage.getItem("bz");
    this._bz = bz !== null ? JSON.parse(bz).bz : undefined;
  }

  setBezirk(bz: DistrictType | undefined) {
    this.bz = bz;
  }

  getOverview(listType: ListType = ListType.CURRENT, bz?: DistrictType): Observable<Overview | undefined> {
    return this.http.get(URL_FOR_LIST(listType) + GET_BZ(listType === ListType.LAST_2_DAYS ? undefined : bz ?? this.bz!), {headers: this.headers, responseType: 'text'})
    .pipe(map(data => {
      if (data.endsWith("();")) {
        return undefined;
      }

      const overview = JSON.parse(data.substr(this.precount, data.length-this.postcount)) as Overview;
      const einsaetze: Array<EinsatzArrayItem> = [];
      let doStuff = true
      for(let i = 0; doStuff; i++) {

        if (overview?.einsaetze === undefined || overview?.einsaetze?.length == 0 || overview?.einsaetze[i]?.einsatz === undefined) {
          doStuff = false
        } else {
          const item = overview.einsaetze[i];
          item.einsatz.startzeit = new Date(item.einsatz.startzeit)
          const fw: Array<any> = [];

          Object.values(item.einsatz.feuerwehrenarray as any).forEach(feuerwehr => {
            fw.push(feuerwehr);
          })
          item.einsatz.feuerwehrenarray = fw;
          einsaetze.push(item)
        }
      }
      overview.einsaetze = einsaetze.filter((e) => listType === ListType.LAST_2_DAYS && (bz ?? this.bz) !== undefined ? e.einsatz.bezirk.id === (bz ?? this.bz) : true).sort((b, a) => a.einsatz.startzeit.getTime() - b.einsatz.startzeit.getTime());
      if (listType === ListType.LAST_2_DAYS) {
        overview.cnt_einsaetze = overview.einsaetze.length;
        overview.cnt_feuerwehren = new Set(overview.einsaetze.map(e => { return e.einsatz.feuerwehrenarray}).reduce((acc, val) => acc.concat(val), []).map(f => f.fwnr)).size

      }
      return overview;
    }));
  }

  getDetails(id: string): Observable<Details> {
    return this.http.get(URL_FOR_DETAILS(id), {headers: this.headers, responseType: 'text'}).pipe(map(data => JSON.parse(data.substr(this.precount, data.length-this.postcount)) as Details));
  }

  getCount(listType: ListType = ListType.CURRENT, bz?: DistrictType): Observable<number | undefined>{
    if (listType === ListType.LAST_2_DAYS && bz !== undefined) {
      return of(undefined);
    }

    return this.http.get(URL_FOR_LIST(listType) + GET_BZ(bz ?? this.bz!), {headers: this.headers, responseType: 'text'}).pipe(
      map((data) => data.endsWith("();") ? undefined : (JSON.parse(data.substr(this.precount, data.length-this.postcount)) as Overview).cnt_einsaetze as number)
    )
  }
}
