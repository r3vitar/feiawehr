export const enum ListType {
    CURRENT = "laufend",
    LAST_6_HOURS = "6stunden",
    TODAY = "taeglich",
    LAST_2_DAYS = "2tage"
}

export enum Nav {
    CURRENT = "/" + ListType.CURRENT,
    LAST_6_HOURS = "/" + ListType.LAST_6_HOURS,
    TODAY = "/" + ListType.TODAY,
    LAST_2_DAYS = "/" + ListType.LAST_2_DAYS
}

export const NAV_FOR_LISTTYPE = (listType: ListType): Nav => {
    switch(listType) {
        case ListType.CURRENT: return Nav.CURRENT;
        case ListType.LAST_6_HOURS: return Nav.LAST_6_HOURS;
        case ListType.TODAY: return Nav.TODAY;
        case ListType.LAST_2_DAYS: return Nav.LAST_2_DAYS;
    }
}

const URL = "http://intranet.ooelfv.at/webext2/";
const URL_API = `${URL}api/`;

const GET_URL_SUFFIX = (): string => {
    const ts = "callback" //new Date().getTime();
    return `&callback=${ts}`;
}

export const GET_BZ = (bz: DistrictType | undefined): string => {
    return bz !== undefined ? `&bz=${bz}` : '';
}

export const URL_FOR_LIST = (timeType: ListType): string => `${URL_API}getjson.php?scope=${timeType}${GET_URL_SUFFIX()}`;
export const URL_FOR_DETAILS = (id: string) => `${URL}detail.php?output=json&NUM1=${id}${GET_URL_SUFFIX()}`

export const isToday = (time: Date): boolean => {
    const now = new Date(Date.now());
    return now.getDate() === time.getDate() && now.getMonth() === time.getMonth()
}

export enum DistrictType {
    BR,
    EF,
    FR,
    GM,
    GR,
    KD,
    LL,
    PE,
    RI,
    RO,
    SD,
    SL,
    UU,
    VB,
    WL,
    ST,
    W,
    L
}

export const districts = [
    DistrictType.BR,
    DistrictType.EF,
    DistrictType.FR,
    DistrictType.GM,
    DistrictType.GR,
    DistrictType.KD,
    DistrictType.LL,
    DistrictType.PE,
    DistrictType.RI,
    DistrictType.RO,
    DistrictType.SD,
    DistrictType.SL,
    DistrictType.UU,
    DistrictType.VB,
    DistrictType.WL,
    DistrictType.ST,
    DistrictType.W,
    DistrictType.L
]

export const getDistrictName = (district: DistrictType): string => {
    switch (district) {
        case DistrictType.BR: return "Braunau";
        case DistrictType.EF: return "Eferding";
        case DistrictType.FR: return "Freistadt";
        case DistrictType.GM: return "Gmunden";
        case DistrictType.GR: return "Grieskirchen";
        case DistrictType.KD: return "Kirchdorf";
        case DistrictType.LL: return "Linz Land";
        case DistrictType.PE: return "Perg";
        case DistrictType.RI: return "Ried";
        case DistrictType.RO: return "Rohrbach";
        case DistrictType.SD: return "Schärding";
        case DistrictType.SL: return "Steyr Land";
        case DistrictType.UU: return "Urfahr Umgebung";
        case DistrictType.VB: return "Vöcklabruck";
        case DistrictType.WL: return "Wels Land";
        case DistrictType.ST: return "Steyr";
        case DistrictType.W: return "Wels";
        case DistrictType.L: return "Linz";
        
    }
    return "";
};

export interface BaseRequest {
    cnt_einsaetze: number;
    cnt_feuerwehren: number;
    title: string;
    version: string;
    webext2: boolean;
}

export type EinsatzArrayItem = {einsatz: Einsatz};

export interface Overview extends BaseRequest {
    einsaetze: Array<EinsatzArrayItem>;
    pubDate: Zeit;
}

export interface Details extends BaseRequest {
    cache: string;
    einsaetze: Array<Einsatz> & {pubDate: Zeit};
    resources: Array<Resource>;
}

export interface Einsatz {
    adresse: Adress;
    alarmstufe: string;
    bezirk: Bezirk;
    cntfeuerwehren: number;
    einsatzart: string;
    einsatzort: string;
    einsatzsubtyp: Typ;
    einsatztyp: Typ;
    feuerwehren: {[fnumber: number]: {feuerwehr: string}};
    feuerwehrenarray: Array<{
        fwname: string;
        fwnr: number;
    }>;
    inzeit: Date | "";
    num1: DetailId;
    startzeit: Date;
    status: string;
    wgs84: Location;
}

export interface Resource {
    bezirk: Bezirk;
    id: string;
    lat: number;
    lng: number;
    name: string; //FF Name
    usedat: Array<{
        id: string;
        inzeit: Zeit;
        startzeit: Zeit;
    }>
    www: string;
}

export interface Bezirk {
    id: DistrictType;
    text: string;
}

export interface Adress {
    default: string;
    earea: string;
    ecompl: string;
    efeanme: string;
    emun: string;
    estnum: string;
}

export interface Typ {
    id: string;
    text: string;
}

export type DetailId = string;
export type Zeit = Date | "";

export type Location = {lat: number; lng: number};

export const filterTextInList = (e: Array<Einsatz>, f: string | undefined): Array<Einsatz> => {
    if(f === undefined || f === null) {
      return e;
    }

    return e.filter((einsatz) => 
    einsatz.einsatzort.toLowerCase().includes(f.toLowerCase()) 
    || einsatz.einsatzsubtyp.text.toLowerCase().includes(f.toLowerCase())
    );
  }

export const filterOrtInList = (e: Array<Einsatz>, o: string | undefined): Array<Einsatz> => {
    if(o === undefined || o === null) {
      return e;
    }

    return e.filter((einsatz) => 
    einsatz.adresse.emun.toLowerCase().includes(o.toLowerCase()));
  }


export const filterStatusInList = (e: Array<Einsatz>, s: string | undefined): Array<Einsatz> => {
    if(s === undefined || s === null) {
        return e;
    }

    return e.filter((einsatz) => 
        einsatz.status === s);
}

export const filterTypInList = (e: Array<Einsatz>, typ: Typ | undefined): Array<Einsatz> => {
    if(typ === undefined || typ === null) {
        return e;
    }

    return e.filter((einsatz) => 
        einsatz.einsatzart === typ.id || einsatz.einsatzsubtyp.id === typ.id);
}

export const filterFeuerwehr = (e: Array<Einsatz>, ff: string | undefined): Array<Einsatz> => {
    if(ff === undefined || ff === null) {
        return e;
    }

    return e.filter((einsatz) => 
        einsatz.feuerwehrenarray.some((f) => f.fwname == ff));
}

