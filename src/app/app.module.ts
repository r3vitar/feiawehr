import { LOCALE_ID, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { OverviewComponent } from './overview/overview.component';
import { DetailComponent } from './detail/detail.component';
import { NavigationComponent, NavList, NavOther, NavTitle } from './navigation/navigation.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatSelectModule } from '@angular/material/select';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatChipsModule } from '@angular/material/chips';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatBadgeModule } from '@angular/material/badge';
import { MatInputModule } from '@angular/material/input';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { MatExpansionModule,} from '@angular/material/expansion';
import { MatListModule } from '@angular/material/list';
import { FourOFourComponent } from './four-ofour/four-ofour.component';
import { DataService } from './data.service';
import { HttpClientModule } from '@angular/common/http';


@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    OverviewComponent,
    DetailComponent,
    NavList,
    NavTitle,
    NavOther,
    FourOFourComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatCardModule,
    MatSelectModule,
    MatExpansionModule,
    MatChipsModule,
    MatFormFieldModule,
    MatBadgeModule,
    MatInputModule,
    MatProgressSpinnerModule,
    FormsModule, ReactiveFormsModule, HttpClientModule
  ],
  providers: [DataService, HttpClientModule,
  { provide: LOCALE_ID, useValue: "de-AT" }

    ],
  bootstrap: [AppComponent],
})
export class AppModule{}
