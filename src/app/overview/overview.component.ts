import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { HttpParams } from '@angular/common/http';
import { ThrowStmt } from '@angular/compiler';
import { AfterContentInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit, AfterViewInit, AfterViewChecked, AfterContentChecked} from '@angular/core';
import { ActivatedRoute, ActivatedRouteSnapshot, Params, Router } from '@angular/router';
import { from, Observable, of, Subject, Subscription } from 'rxjs';
import { filter, map, shareReplay, tap } from 'rxjs/operators';
import { DataService } from '../data.service';
import { Einsatz, EinsatzArrayItem, filterFeuerwehr, filterOrtInList, filterStatusInList, filterTextInList, filterTypInList, isToday, ListType, Nav, NAV_FOR_LISTTYPE, Overview, Typ } from '../stuff';

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.scss']
})
export class OverviewComponent implements OnInit, OnDestroy {

  listType: ListType = this.route.snapshot.data["listType"] ?? ListType.CURRENT;

  readonly CURRENT = ListType.CURRENT;

  subs: Subscription[] = [];

  isDetail?: boolean = this.route.snapshot.data["detail"] ?? false;

  selectedEinsatz?: string;

  detailId?: Observable<string>;

  private _selectedStatus?: string;

  get selectedStatus(): string | undefined {
    return this._selectedStatus;
  }

  set selectedStatus(status: string | undefined) {
    this._selectedStatus = this.listType === this.CURRENT ? undefined : status;
    this.routeQuery({status: this.selectedStatus});
  }

  overView?: Overview;

  einsatzTypen?: Array<Typ>;

  orte?: Array<string>

  private _selectedEinsatzType?: Typ;

  get selectedEinsatzType(): Typ | undefined {
    return this._selectedEinsatzType;
  }

  set selectedEinsatzType(type: Typ | undefined) {
    this._selectedEinsatzType = type
    this.routeQuery({type: this._selectedEinsatzType?.id});
  }

  feuerwehren?: Array<{fwname: string, fwnr: number}>;

  private _selectedFeuerwehr?: {fwname: string, fwnr: number};

  get selectedFeuerwehr(): {fwname: string, fwnr: number} | undefined {
    return this._selectedFeuerwehr;
  }

  set selectedFeuerwehr(ff: {fwname: string, fwnr: number} | undefined) {
    this._selectedFeuerwehr = ff;
    this.routeQuery({ff: this._selectedFeuerwehr?.fwnr});
  }

  private _selectedOrt?: string;

  get selectedOrt(): string | undefined {
    return this._selectedOrt
  }

  set selectedOrt(ort: string | undefined) {
    this._selectedOrt = ort;
    this.routeQuery({ort})
  }
  
  einsaetze2: Observable<Array<Einsatz>> = this.data.getOverview(this.listType).pipe(
    tap(overview => {
      this.overView = overview;
      this.einsatzTypen = [];
      this.feuerwehren = [];
      this.orte = [];
      this.overView?.einsaetze.forEach((einsatz) => {
        const einsatzTyp: Typ = einsatz.einsatz.einsatzart === "TEE" ? einsatz.einsatz.einsatzsubtyp : {id: einsatz.einsatz.einsatzart, text: einsatz.einsatz.einsatzart};
        if (!this.einsatzTypen?.some((typ => typ.id === einsatzTyp.id))) {
          this.einsatzTypen?.push(einsatzTyp)
        }
        if(!this.orte?.includes(einsatz.einsatz.adresse.emun)) {
          this.orte?.push(einsatz.einsatz.adresse.emun);
        }
        for (let feuerwehr of einsatz.einsatz.feuerwehrenarray) {
          if (!this.feuerwehren?.some(f => f.fwnr === feuerwehr.fwnr)) {
            this.feuerwehren?.push(feuerwehr);
          }
        }
      })
      this.orte.sort()
      this.einsatzTypen = this.einsatzTypen?.sort((a, b) => a.text.localeCompare(b.text))
      this.feuerwehren.sort((a, b) => a.fwname.localeCompare(b.fwname));
      const params = this.route.snapshot.queryParams;    
      this._selectedStatus = this.listType === this.CURRENT ? undefined : params.status;
      this._selectedFeuerwehr = this.feuerwehren ? this.feuerwehren?.find((f) => f.fwnr === params.ff) ?? this.routeQuery({ff: undefined}) : undefined;
      this._selectedEinsatzType = this.einsatzTypen ? this.einsatzTypen.find((t) => t.id == params.type) ?? this.routeQuery({type: undefined}) : undefined
      this._filterText = params.search;
      this._selectedOrt = params.ort;
    }),
    map(overview => overview ? overview.einsaetze.map(einsatz => {
      return einsatz.einsatz
    }) : []) 
  );

  private _filterText?: string;

  get filterText(): string | undefined{
    return this._filterText;
  }

  set filterText(filterText: string | undefined) {
    const text = filterText === '' ? undefined : filterText;
    this._filterText = text;
    this.routeQuery({search: text})
  }

  filterList(einsaetze: Array<Einsatz> | null | undefined) {
    if(einsaetze === undefined || einsaetze === null) {
      return einsaetze;
    }
    
    let list = einsaetze;
    list = filterTextInList(list, this.filterText)
    list = filterStatusInList(list, this.selectedStatus)
    list = filterTypInList(list, this.selectedEinsatzType);
    list = filterFeuerwehr(list, this.selectedFeuerwehr?.fwname);
    list = filterOrtInList(list, this.selectedOrt);
    return list;
  }

  isToday = isToday;

  constructor(private route: ActivatedRoute,
    private router: Router,
    private breakpointObserver: BreakpointObserver,
    private data : DataService) { 
      this.router.onSameUrlNavigation = 'reload'
      this.data.bezirkChangeHandler.push(() => {
        this.subs.push(from(this.reload(this.listType, this.route.snapshot)).subscribe());
      })
  }

  ngOnDestroy(): void {
    this.subs.filter((sub) => sub !== undefined && sub !== null).forEach(sub => sub.unsubscribe());
  }

  log(item: any) {
}

routeQuery(param : {[key: string]: any}): any | undefined {
  this.router.navigate([], {
    relativeTo: this.route,
    queryParams: {...param},
    queryParamsHandling: 'merge',
    skipLocationChange: false,
  })
  return undefined;
}

async reload(url: string, snapshot: ActivatedRouteSnapshot): Promise<boolean> {
  await this.router.navigateByUrl('.', { skipLocationChange: true });
  return this.router.navigateByUrl(url).then(() => this.router.navigate([], {
    relativeTo: this.route,
    queryParams: snapshot.queryParams,
  }));
}

  ngOnInit(): void {
    
  }

  
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );
  

    isTooLong(text: string, alt: string): string {
      return text.length > 20 ? alt : text;
    }

}
