import { Component, ContentChild, Directive, Output } from "@angular/core";
import { Observable} from "rxjs";
import { map, shareReplay } from 'rxjs/operators';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';

/** Title of a card, needed as it's used as a selector in the API. */
@Directive({
  selector: `nav-title, [nav-title], [navTitle]`,
  exportAs: 'navTitle',
  host: {
    class: 'nav-title',
  },
})
export class NavTitle {}

@Directive({
  selector: `nav-list, [nav-list], [navList]`,
  exportAs: 'navList',
  host: {
    class: 'nav-list',
  },
})
export class NavList {}

@Directive({
  selector: `nav-other, [nav-other], [navOther]`,
  exportAs: 'navOther',
  host: {
    class: 'nav-other',
  },
})
export class NavOther {}

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent {

  @ContentChild(NavTitle) _title: NavTitle | undefined;

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

  constructor(private breakpointObserver: BreakpointObserver) {}

}
