import { NgModule } from '@angular/core';
import { Route, RouterModule, Routes } from '@angular/router';
import { DetailComponent } from './detail/detail.component';
import { FourOFourComponent } from './four-ofour/four-ofour.component';
import { OverviewComponent } from './overview/overview.component';
import { ListType } from './stuff';

const getChildForType = (listType?: ListType): Route => {
  const child = {
    path: ":id",
    component: DetailComponent,
    data: {
        listType,
        detail: true
    }
  }

  return {
    path: "detail",
    children: [child]
  }
  
};

const getOverviewRouteForType = (listType: ListType): Route => {
  return {
    path: listType,
    component: OverviewComponent,
    data: {
      listType: listType
    },
    children: [getChildForType(listType)]
  }
}

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: ListType.CURRENT,
  }, 
  getOverviewRouteForType(ListType.CURRENT),
  getOverviewRouteForType(ListType.LAST_2_DAYS),
  getOverviewRouteForType(ListType.LAST_6_HOURS),
  getOverviewRouteForType(ListType.TODAY),
  getChildForType(),
  {
    path: '**',
    component: FourOFourComponent
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes, {onSameUrlNavigation: "reload"})],
  exports: [RouterModule]
})
export class AppRoutingModule {
  
  constructor() {
  }
 }
